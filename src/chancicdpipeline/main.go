package main

import (
  "fmt"
	"io"
  "os"
	"net/http"
)

func getHelloWorldString() (str string){
  str = "Hello DevCamp"
  return
}

func respondToRequest(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, getHelloWorldString())
}

func main() {
	http.HandleFunc("/", respondToRequest)
  port := os.Getenv("PORT")
  fmt.Println(port)
	err := http.ListenAndServe(":"+port, nil)
  if err != nil {
    fmt.Println(err)
  }
}
