package main

import "testing"

func TestHelloWorldString(t *testing.T){
  if getHelloWorldString() != "Hello DevCamp"{
    t.FailNow()
  }
}
